Download the latest version of the mod loader from the home page.

The installer will explain where you need to install the files for the mod loader.

To launch the game with mods, you need to navigate to the folder called "VTOLVR_ModLoader" and run "VTOLVR-ModLoader.exe" or you can use a shortcut created with the installer.
To uninstall just delete the folder "VTOLVR_ModLoader" and the core mod loader files should be removed.

https://youtu.be/qLPAgBOHK-E