Now we have downloaded the correct software needed to create mods, we can start making them! 
# Creating the project
The mod loader provides a template that you can use to get up and running with a blank mod. In the mod creator tab, press the new project button.

![](/images/creating-a-mod/Creating-a-new-project.png)

Then you'll be able to write out the name of your mod and, it's description (which can be edited later). Make sure Mod is selected in the dropdown at the top right of the window. 

![](/images/creating-a-mod/Creating-Project.png)

Pressing create will download the latest template and fill out the necessary files needed.

We highly encourage open-sourcing your mods, this helps the community learn and makes it able for people to share feedback. This is why the template project is set up in a way ready to be posted to a new git repository. It includes a .gitingore file, a builds folder and a dependencies folder with instructions.

```
VTOLVR Tutorial Mod (Folder Structure)
    Builds
        info.json
    Dependencies
        Assembly CSharp.dll
        instructions.txt
        ModLoader.dll
        ModLoader.xml
        UnityEngine.CoreModule.dll
        UnityEngine.dll
    VTOLVRTutorialMod
        Properties
            AssemblyInfo.cs
        Main.cs
        VTOLVRTutorialMod.csproj
    .gitignore
    VTOLVRTutorialMod.sln
```

From here you can press Open Project to open it in visual studio, Edit Info to edit the information about the mod such as name, description, images etc. 

![](/images/creating-a-mod/Project-Created.png)

# Time to add something into the game!

So with what we currently have, it will inject into the game our script but nothing else happens. Let's make a simple button output to the log so we know it's working fine. 


Our first bit of code will go in `Main.cs.` We're just going to do something simple and log a message saying hello world.

Inside of ModLoaded, we'll place a Log() message. ModLoaded gets called when the player has pressed the loaded button and the mod loader has finished setting up your mod.
I haven't gone through this code in detail because this is something people with experience in Unity should know. If this is the first time getting into programming, I recommend at this step, looking up unity tutorials, any Unity tutorial would apply to this however it does become a bit harder when applying it to this situation of modding.

```cs
public override void ModLoaded()
{
    //This is an event the VTOLAPI calls when the game is done loading a scene
    VTOLAPI.SceneLoaded += SceneLoaded;
    base.ModLoaded();
    Log("HELLO WORLD!!!!!!");
}
```

# Building the .dll file

Now we have our mod create, we need to create the .dll file. Thankfully this is simple, right clicking on the project and pressing Build. 

![](/images/creating-a-mod/Build-Button.PNG)

# Completed our first mod!

So now when you launch the game and head into the mods tab. You should see your mod at the bottom of the list. And when you load it, the Log() method will be run, and output your message inside of the player.log file. 

![](/images/creating-a-mod/Your-mod-in-game.png)
![](/images/creating-a-mod/The-log-message-in-the-playerlog.png)

Once you have finished creating your mod, head over to the release section to learn how to add an image and description to your mod. 