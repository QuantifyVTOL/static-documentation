Run this to patch your methods! https://github.com/pardeike/Harmony/wiki/Bootstrapping
```cs
private void Start()
{
    HarmonyInstance instance = HarmonyInstance.Create("me.mymod");
    instance.PatchAll(Assembly.GetExecutingAssembly());
}
```