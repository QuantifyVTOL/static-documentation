This returns the game object of the vehicle which the player is flying if the vehicle was not found, it will just return null so make sure to have a null check! 
```cs
GameObject currentVehicle = VTOLAPI.GetPlayersVehicleGameObject();
```