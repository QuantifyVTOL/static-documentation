Run code when the scene has finished loading. 
```cs
public override void ModLoaded()
{
    VTOLAPI.SceneLoaded += SceneLoaded;
    base.ModLoaded();
}

private void SceneLoaded(VTOLScenes scene)
{
    switch (scene)
    {
        case VTOLScenes.ReadyRoom:
            break;
        case VTOLScenes.Akutan:
        case VTOLScenes.CustomMapBase:
            Log("Map Loaded");
            break;
        case VTOLScenes.LoadingScene:
            break;
    }
}
```