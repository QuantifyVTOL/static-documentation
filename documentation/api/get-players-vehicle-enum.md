This returns an enum to help identify which vehicle the player is currently using. This can be used if you have specific code to run depending on what vehicle is being used. 
```cs
VTOLVehicles currentVehicle = VTOLAPI.GetPlayersVehicleEnum(); 
//Possible Returns: None, AV42C, FA26B, F45A
```