GetSteamName returns the users current steam name which is on their steam profile, please remember though this name can be changed by the player at any time. This function uses Steamworks and returns as a string.
```cs
Debug.Log("This is the users Steam Name: " + VTOLAPI.instance.GetSteamName())
```